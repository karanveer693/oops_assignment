from abc import ABC, abstractmethod

class Product:
    def __init__(self, name, price):
        self.name = name
        self.price = price

    def get_price(self):
        return self.price

class Order(ABC):
    @abstractmethod
    def add_product(self, product):
        pass

    @abstractmethod
    def remove_product(self, product):
        pass

    @abstractmethod
    def get_total(self):
        pass

class Customer:
    def __init__(self, name, email):
        self.name = name
        self.email = email

class Shipper(ABC):
    @abstractmethod
    def ship_order(self, order):
        pass

class FedEx(Shipper):
    def ship_order(self, order):
        # logic to ship order via FedEx
        pass

class UPS(Shipper):
    def ship_order(self, order):
        # logic to ship order via UPS
        pass

class OnlineOrder(Order):
    def __init__(self, customer):
        self.customer = customer
        self.products = []

    def add_product(self, product):
        self.products.append(product)

    def remove_product(self, product):
        self.products.remove(product)

    def get_total(self):
        total = 0
        for product in self.products:
            total += product.get_price()
        return total

class InStoreOrder(Order):
    def __init__(self):
        self.products = []

    def add_product(self, product):
        self.products.append(product)

    def remove_product(self, product):
        self.products.remove(product)

    def get_total(self):
        total = 0
        for product in self.products:
            total += product.get_price()
        return total
